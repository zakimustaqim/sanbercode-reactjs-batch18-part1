// SOAL 1
var deret = 0;
var penambah = 2;

console.log("LOOPING PERTAMA");
while(deret < 20){
	deret += penambah;
	console.log(deret + " - I love coding");
}

console.log("LOOPING KEDUA");
while(deret > 0){
	console.log(deret + " - I will become a frontend developer");
	deret -= penambah;
}


// SOAL 2
for(var deret = 1; deret <=20; deret++ ){
	if(deret%3 == 0 && deret%2 == 1){
		console.log(deret + " - I Love Coding");
	}else if(deret%2 == 1){
		console.log(deret + " - Santai");
	}else if(deret%2 == 0){
		console.log(deret + " - Berkualitas");
	}
}


// SOAL 3
var deret = 1;
var cetak = "*";

while(deret < 8){
	console.log(cetak)
	cetak += "*";
	deret++;
}


// SOAL 4
var kalimat = "saya sangat senang belajar javascript";
var hasil = kalimat.split(" ");
console.log(hasil);


// SOAL 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
var lenarray = daftarBuah.length;

for(var i=0; i < lenarray; i++){
	console.log(daftarBuah[i]);
}