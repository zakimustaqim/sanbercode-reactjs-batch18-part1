// SOAL 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
console.log(kataPertama.concat(" ").concat(kataKedua.charAt(0).toUpperCase()).concat(kataKedua.substring(1)).concat(" ").concat(kataKetiga).concat(" ").concat(kataKeempat.toUpperCase()));


// SOAL 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
console.log(parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat));


// SOAL 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


// SOAL 4
var nilai = 99;

if ( nilai >= 80 ) {
    console.log("indeks A")
} else if ( nilai >= 70 && nilai < 80 ) {
    console.log("indeks B")
} else if ( nilai >= 60 && nilai < 70 ) {
    console.log("indeks C")
} else if ( nilai >= 50 && nilai < 60 ) {
    console.log("indeks D")
} else if ( nilai < 50 ) {
    console.log("indeks E")
}


// SOAL 5
var tanggal = 25;
var bulan = 6;
var tahun = 1997;

switch(bulan){
	case 1: {bulan = "Januari"; break;}
	case 2: {bulan = "Februari"; break;}
	case 3: {bulan = "Maret"; break;}
	case 4: {bulan = "April"; break;}
	case 5: {bulan = "Mei"; break;}
	case 6: {bulan = "Juni"; break;}
	case 7: {bulan = "Juli"; break;}
	case 8: {bulan = "Agustus"; break;}
	case 9: {bulan = "September"; break;}
	case 10: {bulan = "Oktober"; break;}
	case 11: {bulan = "November"; break;}
	case 12: {bulan = "Desember"; break;}
	default: {bulan = "Bulan Belum Ditentukan";}
}

console.log(tanggal + " " + bulan + " " + tahun)
