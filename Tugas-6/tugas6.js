// SOAL 1
const r = 7;
const phi = 3.14;
const hasil = 0;

const luasLingkaran = (r) => {
	let phi = 22/7
	let hasil = phi * r * r;
	return hasil;
}

const kelilingLingkaran = (r) => {
	let phi = 22/7
	let hasil = 2 * phi * r;
	return hasil
}

console.log(luasLingkaran(r));
console.log(kelilingLingkaran(r));
// console.log(phi);
// console.log(hasil);


// SOAL 2
let kalimat = "";

const tambah = (kata) =>{
	kalimat = `${kalimat}${kata}`  
}

tambah("saya ");
tambah("adalah ");
tambah("seorang ");
tambah("frontend ");
tambah("developer ");

console.log(kalimat);


// SOAL 3
const newFunction = function literal(firstName, lastName){
  return {
	firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
newFunction("William", "Imoh").fullName()


// SOAL 4
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation);


// SOAL 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west,...east]

console.log(combined)