// SOAL 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
] 

var time = 10000;
var i = 0;
const execute = (time, i) => readBooksPromise(time, books[i])
	.then(function(sisawaktu){
		i+=1;
		if(time > 0 && (i < books.length)){
			execute(sisawaktu, i);
		}
	})
	.catch(function(sisawaktu){})

execute(time, i);