// SOAL 1
function hello(){
	return "Halo Sanbers!";
}

console.log(hello());


// SOAL 2
function kalikan(paramPertama, paramKedua){
	var hasil = paramPertama * paramKedua;
	return hasil;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2)
console.log(hasilKali)


// SOAL3
function introduce(name, age, address, hobby) {
	return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
}

var name = "Jhon";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);


// SOAL 4
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var objectDaftarPeserta = {};
objectDaftarPeserta.nama = arrayDaftarPeserta[0];
objectDaftarPeserta["jenis kelamin"] = arrayDaftarPeserta[1];
objectDaftarPeserta.hobi = arrayDaftarPeserta[2];
objectDaftarPeserta["tahun lahir"] = arrayDaftarPeserta[3];
console.log(objectDaftarPeserta);


// SOAL5
var arrayOfObjectDaftarBuah =[{nama: "strawberry", warna: "merah", "ada bijinya": "tidak", "harga": 9000}, {nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000}, {nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada",  harga: 10000}, {nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 5000}]
console.log(arrayOfObjectDaftarBuah[0]);


// SOAL 6
var dataFilm = [];
function tambahFilm(nama, durasi , genre, tahun) {
	var index = dataFilm.length;
	var objectDataFilm = {};
	objectDataFilm.nama = nama;
	objectDataFilm.durasi = durasi;
	objectDataFilm.genre = genre;
	objectDataFilm.tahun = tahun;
	dataFilm[index] = objectDataFilm;
}

tambahFilm("Miracle in Cell No. 7", "2 jam 7 menit", "Komedi/Drama", "2013");
tambahFilm("Interstellar", "2 jam 47 menit", "Fiksi ilmiah/Petualang", "2014");
console.log(dataFilm);
