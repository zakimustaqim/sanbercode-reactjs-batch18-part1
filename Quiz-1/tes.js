// SOAL 11
var daftarAlatTulis = ["2. Pensil", "5. Penghapus", "3. Pulpen", "4. Penggaris", "1. Buku"]
daftarAlatTulis.sort();
var i = 0;
while(i < daftarAlatTulis.length){
	console.log(daftarAlatTulis[i]);
	i++;
}


// SOAL 12
var pesertaLomba= [["Budi", "Pria", "172cm"], ["Susi", "Wanita", "162cm"], ["Lala", "Wanita", "155cm"], ["Agung", "Pria", "175cm"]]
var arrayOfObjectPesertaLomba = [];

for(var i=0; i < pesertaLomba.length; i++){
	var objectPesertaLomba = {};
	objectPesertaLomba.nama = pesertaLomba[i][0]
	objectPesertaLomba["jenis kelamin"] = pesertaLomba[i][1]
	objectPesertaLomba["tinggi badan"] = pesertaLomba[i][2]
	arrayOfObjectPesertaLomba.push(objectPesertaLomba);
}

console.log(arrayOfObjectPesertaLomba);


// SOAL 13
function luasLingkaran(r){
	return 22 / 7 * r * r
}

function luasSegitiga(alas , tinggi){
	return 1/2 * alas * tinggi
}

function luasPersegi(sisi1, sisi2){
	return sisi1 * sisi2
}

console.log(luasLingkaran(14));
console.log(luasSegitiga(10, 8));
console.log(luasPersegi(5, 5));


// SOAL 14
var daftarNama = [];

function tambahNama(nama, jenisKelamin){
	var objectDaftarNama = {};
	objectDaftarNama.nama = nama;
	objectDaftarNama["jenis kelamin"] = jenisKelamin;
	if(jenisKelamin == "laki-laki"){
		objectDaftarNama.panggilan = "Bapak"	
	}else if(jenisKelamin == "perempuan"){
		objectDaftarNama.panggilan = "Ibu"
	}
	daftarNama.push(objectDaftarNama);
}

tambahNama("Asep", "laki-laki");
tambahNama("Siti", "perempuan");
tambahNama("Yeni", "perempuan");
tambahNama("Rudi", "laki-laki");
tambahNama("Adit", "laki-laki");

var i = 1;
daftarNama.forEach(function(item){
	console.log(i + ". " + item.panggilan + " " + item.nama);
	i++	
})